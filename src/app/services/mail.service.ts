import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { emailModel } from '../model/email.model';

@Injectable({
  providedIn: 'root'
})
export class MailService {
  url = 'http://dev.dreamloading.com/email.php';

  constructor(private http: HttpClient) { }

  sendEmail(email: emailModel){
    return this.http.post(`${this.url}`, email);
  }
}
