import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MailService } from '../../services/mail.service';
import { emailModel } from '../../model/email.model';
import { AppComponent } from '../../app.component';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  formulario: FormGroup;
  email: emailModel = new emailModel();

  get nameInvalid() {
    return this.formulario.get('name').invalid && this.formulario.get('name').touched;
  }
  get subjectInvalid() {
    return this.formulario.get('subject').invalid && this.formulario.get('subject').touched;
  }
  get emailInvalid() {
    return this.formulario.get('email').invalid && this.formulario.get('email').touched;
  }
  get descrInvalid() {
    return this.formulario.get('description').invalid && this.formulario.get('description').touched;
  }

  constructor(private fb: FormBuilder, private mailService: MailService, public appComponent:AppComponent) {
    this.createForm();
    this.loadDataToForm();
  }

  ngOnInit(): void {
  }

  loadDataToForm(){
    this.formulario.reset({
      name: '',
      subject: '',
      email: '',
      description: ''
    });
  }

  createForm(){
    this.formulario = this.fb.group({
      name: ['', Validators.required],
      subject: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      description: ['', Validators.required]
    });
  }

  saveForm(){
    if (this.formulario.invalid) {
      Object.values(this.formulario.controls).forEach( control => {
        if (control instanceof FormGroup) {
          Object.values(control.controls).forEach( control2 => {
            control2.markAsTouched();
          });
        } else {
          control.markAsTouched();
        }
      });
    } else {
      this.mailService.sendEmail(this.email).subscribe( resp => {
        console.log(resp);
      });
    }
  }
}
