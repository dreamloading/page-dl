import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'app-lang',
  templateUrl: './lang.component.html',
  styleUrls: ['./lang.component.css']
})
export class LangComponent implements OnInit {

  public loading = false;
  public night = false;
  public activeLang = 'es';

  constructor(private translate: TranslateService, private appComponent: AppComponent) {
    this.translate.setDefaultLang(this.activeLang);
  }

  ngOnInit(): void {

  }

  public cambiarLenguaje(lang) {
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
    }, 2000);
    this.activeLang = lang;
    this.translate.use(lang);
  }

  changeMode(){
    this.night = !this.night;
    this.appComponent.darkmode = this.night;
  }
}
