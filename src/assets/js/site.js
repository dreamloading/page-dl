$(function() {"use strict";
	var stickOnScroll;

	//Header Option
	$('#header').addClass('normal');
	//Choose Here Class Name (normal or fixed or intelligent);

	if ($('#owl-slider1').length) {
		$("#owl-slider1").owlCarousel({
			autoPlay : 3000, //Set AutoPlay to 3 seconds
			items : 2,
			itemsDesktop : [1199, 2],
			itemsDesktopSmall : [979, 2],
			itemsMobile : [600, 1]
		});
	}
	if ($("#who-we-are-slider").length) {
		$("#who-we-are-slider").owlCarousel({
			autoPlay : 3000, //Set AutoPlay to 3 seconds
			items : 3,
			itemsDesktop : [1199, 3],
			itemsDesktopSmall : [979, 3],
			itemsTablet : [768, 3],
			itemsMobile : [767, 1]
		});
	}

	if ($('.flexslider').length) {
		$('.flexslider').flexslider({
			animation : "slide"
		});
	}

	if ($('.progress-bar').length) {
		$('.progress-bar').appear(function() {
			$('.progress-bar').each(function() {
				var x = $(this).attr('aria-valuenow');
				$(this).css({
					"width" : x + '%'
				}, 300);
			});
		});
	}

	if ($(window).width() < 768) {

		$('#header.header-one .navbar-nav > li > a').on('click', function() {
		$(this).next('.sub-menu').slideToggle();
		});
	}
	
	if($(window).width() < 768) {
		$('.header-three .one-page-scroll .pages a').on('click',function(){ 
			$(this).next('.sub-menu').slideToggle();
		});
	}

	$('#header .nav').find('li').on('click', function() {
		$(this).siblings('li').removeClass('active');
		$(this).addClass('active');
	});

	$('.open-info').on('click', function(e) {
		e.preventDefault();
		$(this).parents('.item').addClass('open_slide');
	})

	$('.cross').on('click', function(e) {
		e.preventDefault();
		$(this).parents('.item').removeClass('open_slide');
	})

	$('.top-large-arrow').on('click', function() {
		$('html, body').animate({
			scrollTop : 0
		}, 1000);
	});

	/*===========================scroll function start here====================*/
	
	
	$(window).load(function() {
		
		//=================Header Style function================
		if ($('#header').hasClass('fixed')) {
			$('#header').next().addClass('top-m');
		}
		if ($('#header').hasClass('intelligent')) {
			$('#header').next().addClass('top-m');
		};
	});

	var class_pr = $('body').attr('class');
	var headerHeight = $('#header').outerHeight();
	var st = $(window).scrollTop();
	stickOnScroll = function() {

		if ($('#header').hasClass("intelligent")) {

			$('#header').removeClass('normal');
			$('#header').next().addClass('top-m');
			var pos = $(window).scrollTop();

			if (pos > headerHeight) {
				if (pos > st) {
					$('#header').addClass('simple')
					$('#header.simple').removeClass('down');
					$('#header.simple').addClass('fixed up');

				} else {
					$('#header.simple').removeClass('up');
					$('#header.simple').addClass('fixed down');

				}
				st = pos;

			} else {
				$('#header.simple').removeClass('fixed down up simple');
			}
			if (pos == $(document).height() - $(window).height()) {
				$('#header.simple').removeClass('up');
				$('#header.simple').addClass('fixed down');
			}

		} else if ($('body').hasClass("fix")) {

			$('#header').next().addClass('top-m');
			$('#header').addClass('simple fixed');
			$('#header').removeClass('down up');
			$('#wrapper').css({
				paddingTop : 0
			});
		} else {

			$('#header.simple').removeClass('fixed down up simple');
			$('#header').addClass('normal');
			
			$('#wrapper').css({
				paddingTop : 0
			});
		}
	};
	stickOnScroll();
	$(window).scroll(function() {
		stickOnScroll();
	});

	// end for sticky header
	
	$(window).load(function(){
	$("#loading").fadeOut(500);
	// Filltering.................

});
	

}); 